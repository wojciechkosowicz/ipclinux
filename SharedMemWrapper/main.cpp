#include <iostream>
#include <cstring>
#include <sys/ipc.h>
#include <sys/shm.h>

namespace MyShared {
struct Message {
    bool newmessage;
    char message[1024];
    Message() : newmessage(false) {
        memset(message, 0, 1024);
    }
};

bool createActivateShdMemory(int memId) {
    int result = shmget((key_t) memId, sizeof(Message), 0666 | IPC_CREAT);
    if (result == -1) {
        std::cout << "Error while creating shared memory";
        return false;
    }
    shmat(result, (void*) 0, 0);
//    if (result == -1) {
//        std::cout << "Error while managing memory";
//        return false;
//    }
    return true;

}

} // MyShared
