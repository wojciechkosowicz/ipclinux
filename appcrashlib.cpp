class AppCrashHandler {
  public:

    pthread_t watchdog_thread;
    time_t seconds_diff;
    void Init();
    time_t readSecondsDiff();
    void setSecondsDiff(time_t timeToSet);
    pthread_mutex_t threadMutex;
};

void signal_callback_handler(int signum)
{
    std::cout << "Handling signal callback";
}

void* watchdog_function(void* arg) {
    AppCrashHandler* appCrashHandler = (AppCrashHandler*) arg;
    while(true) {
        time_t current_timestamp = time(NULL);
        time_t from_thread = appCrashHandler->readSecondsDiff();
        time_t diff = current_timestamp - from_thread;
        if (diff > 120) {
            //raise(SIGINT);
        } else {
            std::cout << "Diff was " << diff;
        }
    }
}

void AppCrashHandler::Init() {
    signal(SIGINT, signal_callback_handler);
    int result = pthread_mutex_init(&threadMutex, NULL);
    if (result != 0) {
        std::cout << "Failure at mutex initialization";
    }
    result = pthread_create(&watchdog_thread, NULL, watchdog_function, (void*) this);
    if (result != 0) {
        std::cout << "Error while creating thread";
    }
}

time_t AppCrashHandler::readSecondsDiff() {
    pthread_mutex_lock(&threadMutex);
    time_t value = seconds_diff;
    pthread_mutex_unlock(&threadMutex);
    return value;
}

void AppCrashHandler:: setSecondsDiff(time_t timeToSet) {
    pthread_mutex_lock(&threadMutex);
    seconds_diff = timeToSet;
    pthread_mutex_unlock(&threadMutex);
}